#pragma once
#include "View.hpp" 
/*!	\file		GomokuViews.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 Contains the View classes Gomoku game. They are the oberver in observer pattern
*/

class GameBoardView : public IObserver
{ 
	DrawDotCommand ddcmd; 
public:
	GameBoardView();
	const  int RANGE_COLUMN = 38 + 4 + 34;

	ConsoleApplication* app;
	//this is a command receiver
	virtual void update(ViewInfo value);

	//call this to display the view? 
	virtual void show();

	//why not use constructor to initialize?
	virtual void initialize();

	string isMe(int r, int c);
};


class NameView : public IObserver
{
public:
	const WORD   START_Y = 1;
	const WORD   START_X = 80;
	const WORD   EDIT_SECTION_ROW = 15;
	const WORD   EDIT_SECTION_COL = 29;
	const WORD   COLOR = 0x0020;

	const COORD   EDIT_CONTROL_START = COORD{ 86,  6 };
	const WORD   EDIT_CONTROL_LENGTH = 10;
	const WORD   EDIT_CONTROL_ATTR = 0x0010 | 0x0020 | 0x0040 | 0x0004;//0x0004 is fore-red

	const COORD EXPLAIN = COORD{ 83,2 };
	const COORD P1 = COORD{ 85,4 };
	const COORD P2 = COORD{ 95,4 };
	const COORD P1COLOR = COORD{ 87,5 };
	const COORD P2COLOR = COORD{ 95,5 };

	const COORD CONFIRM = COORD{ 99,6 };

	DrawDotCommand dotcmd;
	DrawEditControlCommand editccmd;
	OutputStrCommand strcmd;
public:
	ConsoleApplication* app;
	//this is a command receiver
	virtual void update(ViewInfo value);

	//call this to display the view 
	virtual void show();
	 
	virtual void initialize();

	string isMe(int r, int c);
};

class ScoreView : public IObserver
{
	const WORD   START_Y = 15;
	const WORD   START_X = 80;  
	const WORD   SECTION_ROW = 5;
	const WORD   SECTION_COL = 29;
	const string   COLOR_STR = "BLUE";
	const DWORD SCORE_COLOR = 0x0010 | 0x0001 | 0x0002 | 0x0004;
	
	OutputStrCommand strcmd;
	DrawDotCommand dotcmd;  
public: 
	ScoreView();
	ConsoleApplication* app;
	//this is a command receiver
	virtual void update(ViewInfo value);

	//call this to display the view 
	virtual void show();

	//why not use constructor to initialize?
	virtual void initialize();

	string isMe(int r, int c);
};


class MoveView : public IObserver
{
	const WORD   START_Y = 20;
	const WORD   START_X = 80;
	const WORD   SECTION_ROW = 18;
	const WORD   SECTION_COL = 29;
	const string   COLOR_STR = "WHITE";
	const WORD COLOR = 0x0020 | 0x0040 | 0x0010;// 0x0001 | 0x0002 | 
	const WORD   BUTTON_START_Y = 22;
	const WORD   BUTTON_START_X = 97;

	DrawDotCommand dotcmd;
	OutputStrCommand strcmd; 
public:
	ConsoleApplication* app;
	//this is a command receiver
	virtual void update(ViewInfo value);

	//call this to display the view 
	virtual void show();

	//why not use constructor to initialize?
	virtual void initialize();

	string isMe(int r, int c);
};