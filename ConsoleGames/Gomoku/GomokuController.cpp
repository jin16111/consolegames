#include "GomokuController.hpp"
/*!	\file		GomokuController.hpp
   \author		Jinzhi Yang
   \date		2019-04-18

	GomokuController class methods implementation
*/

GomokuController::GomokuController()
{ 
	gv.app = this->app;
	nv.app = this->app;
	sv.app = this->app;
	mv.app = this->app;
	 

	model.attach(&gv);
	model.attach(&nv); 
	model.attach(&sv);
	model.attach(&mv);
}