#pragma once
/*!	\file		Gomoku.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 Gomoku game defination
*/

#include "ConsoleApplication.hpp"
#include "GomokuController.hpp"
#include "GomokuModel.hpp"

class Gomoku : public ConsoleApplication
{
	GomokuController controller;
	GomokuModel model;

	vector<tuple<int,int, int>> moves;

	bool exit = false; 
	bool gameover = false;

	int p1Longest = 0;
	int p2Longest = 0;
	
	unsigned short p1Color = 0;
	unsigned short p2Color = 0x0040;

	int moveviewBias = 0;
	string viewFocus = "";
	int mouse_x = 0;
	int mouse_y = 0;

	string currentPieceColor = "RED";
	DWORD currentColor = p2Color;
	
	SHORT colors[7] = {0,0x0010,0x0020,0x0040,0x0010|0x0040,0x0010|0x0020,0x0040|0x0020};

	bool confirmedColor = false;

public:
	Gomoku();

	//try to put a piece on the board
	bool putPiece(int gameCol, int gameRow, DWORD color);

	int execute() override;
	
	//method to switch turns
	void ChangeCurrentColor();

	//initialize game
	void initialize();

	//used to check the game result.
	int checkResultHelper(int &x, int &y);
	
	//indicating which direction to check
	int checkDirection(int &x, int &y, string direction);
	
	//help to complete undo function
	void undoHelper();
	
	//refresh scoreview
	void updateScoreView();
}g;
