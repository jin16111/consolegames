#pragma once
#include "Model.hpp"
/*!	\file		GomokuModel.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 GomokuModel cantian a list of view. it is the subject of oberver patterns
*/



class GomokuModel : public Model {
public:
	string p1Name;//currently not in use
	string p2Name;// currently not in use
	vector<tuple<int, int, int>> moves; //currently not in use
};
