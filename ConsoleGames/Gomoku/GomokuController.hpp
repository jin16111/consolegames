#pragma once
#include "GomokuViews.hpp"
#include "GomokuModel.hpp"
/*!	\file		GomokuController.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 GomokuController is the controller of this MVC app. it is responsible for the communication between model and view
*/

class GomokuController
{
	

public:
	ConsoleApplication* app;
	GomokuModel model;
	GameBoardView gv;
	NameView nv;
	ScoreView sv;
	MoveView mv;

	GomokuController(); 
};

