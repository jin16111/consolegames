#include "Gomoku.hpp"
/*!	\file		Gomoku.cpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 Gomoku class methods implementation
*/
  
Gomoku::Gomoku()
{
	controller.app = this;
	setFlag(&exit);
}

///helper functions
//remap column number
int remapCol(int col)
{
	col -= 2;
	return col / 4;
}
//remap row number
int remapRow(int row)
{
	return row / 2;
}
//turn background color to foreground color
int backToFore(int c)
{
	if (c == 0x0010)
		c = 0x0001;
	else if (c == 0x0020)
		c = 0x0002;
	else if (c == 0x0040)
		c = 0x0004;
	else if (c == (0x0010 | 0x0020))
		c = 0x0001 | 0x0002;
	else if (c == (0x0010 | 0x0040))
		c = 0x0001 | 0x0004;
	else if (c == (0x0040 | 0x0020))
		c = 0x0004 | 0x0002;
	return c;
}
//see if a click on border or a valid fild
bool isBorder1(int r, int c)
{
	if (r % 2 == 0 || c % 4 == 0 || (c - 1) % 4 == 0)
		return true;
	else
		return false;
}

void Gomoku::ChangeCurrentColor()
{
	if (currentColor != p2Color && currentColor != p1Color)
		currentColor = p2Color;

	if (currentColor == p2Color)
		currentColor = p1Color;
	else if (currentColor == p1Color)
		currentColor = p2Color;

	//currentColor == p2Color ? currentColor = p1Color : currentColor = p2Color;
}

void Gomoku::initialize()
{
	setTitle("Gomoku");
	//resizeWindow(80 + 34,40);
	resizeWindow(38 + 4 + 34 + 34, 21 * 2 - 3);
	moves.clear();

	ViewInfo show;
	show.name = "show";
	controller.model.notify(show);

	gameover = false;
	moveviewBias = 0;
	p1Longest = 0;
	p2Longest = 0;
	viewFocus = "";
	currentColor = p2Color;
	confirmedColor = false;
	p1Color = 0;
	p2Color = 0x0040;
}

bool Gomoku::putPiece(int gameCol, int gameRow, DWORD color)
{
	////see if this place was taken in list
	if (find(moves.begin(), moves.end(), make_tuple(1, gameCol, gameRow)) == moves.end()
		&&
		find(moves.begin(), moves.end(), make_tuple(2, gameCol, gameRow)) == moves.end()
		)
	{
		if (moves.size() == 0)
		{
			moves.push_back(make_tuple(1, gameCol, gameRow));
			//check the result
			int tempCount = checkResultHelper(gameCol, gameRow);
			p2Longest = tempCount > p2Longest ? tempCount : p2Longest;


		}
		//if not, check the color and call f2 to lay the piece  
		else if (currentColor == p2Color)
		{
			moves.push_back(make_tuple(1, gameCol, gameRow));

			//check the result
			int tempCount = checkResultHelper(gameCol, gameRow);
			p1Longest = tempCount > p1Longest ? tempCount : p1Longest;
			if (tempCount >= 5)
			{
				gameover = true;
				ViewInfo vi;
				vi.color = 0x0040 | 0x0001 | 0x0002 | 0x0004;
				vi.text = "P1 Wins! Press r to restart";
				vi.name = "winner";
				controller.model.notify(vi);
			}
		}
		else
		{
			moves.push_back(make_tuple(2, gameCol, gameRow));
			//check the result
			int tempCount = checkResultHelper(gameCol, gameRow);
			p2Longest = tempCount > p2Longest ? tempCount : p2Longest;

			if (tempCount >= 5)
			{
				gameover = true;
				ViewInfo vi;
				vi.color = 0x0040 | 0x0001 | 0x0002 | 0x0004;
				vi.text = "P2 Wins! Press r to restart";
				vi.name = "winner";
				controller.model.notify(vi);
			}
		}
		ChangeCurrentColor();
		return true;
	}
	else
		return false;

}

int Gomoku::execute() {

	initialize();

	while (!exit) {
		auto inputEvent = startMonitoringInput0();
		auto e = get<0>(inputEvent);
		mouse_x = get<1>(inputEvent);
		mouse_y = get<2>(inputEvent);

		if (e == " ")
		{
			continue;
		}
		else if (e == "ML" && !gameover)
		{
			int c = get<1>(inputEvent);
			int r = get<2>(inputEvent);

			//cout << "c:" << c << ", r" << r << endl;


			//ask who is clicked
			string viewname = controller.model.query(r, c);
			viewFocus = viewname;

			if (!confirmedColor)
			{
				if (viewname == "p1color")
				{
					for (size_t i = 0; i < 7; i++)
					{
						if (colors[i] == p1Color)
						{
							i++;
							if (i >= 7)
								i = 0;
							if (colors[i] != p2Color)
							{
								p1Color = colors[i];
								//
								ViewInfo vpc;
								vpc.name = "p1color";
								vpc.color = p1Color;
								controller.model.notify(vpc);
								break;
							}
							else
							{
								i++;
								if (i >= 7)
									i = 0;
								if (colors[i] != p2Color)
								{
									p1Color = colors[i];
									//
									ViewInfo vpc;
									vpc.name = "p1color";
									vpc.color = p1Color;
									controller.model.notify(vpc);
									break;
								}
							}
						}
					}
				}
				else if (viewname == "p2color")
				{
					for (size_t i = 0; i < 7; i++)
					{
						if (colors[i] == p2Color)
						{
							i++;
							if (i >= 7)
								i = 0;
							if (colors[i] != p1Color)
							{
								p2Color = colors[i];
								//
								ViewInfo vpc;
								vpc.name = "p2color";
								vpc.color = p2Color;
								controller.model.notify(vpc);
								break;
							}
							else
							{
								i++;
								if (i >= 7)
									i = 0;
								if (colors[i] != p1Color)
								{
									p2Color = colors[i];
									//
									ViewInfo vpc;
									vpc.name = "p2color";
									vpc.color = p2Color;
									controller.model.notify(vpc);
									break;
								}
							}
						}
					}
					currentColor = p2Color;
				}
				else if (viewname == "confirm")
				{
					confirmedColor = true;
					ViewInfo vi;
					vi.name = "confirm";
					controller.model.notify(vi);
				}
				if (!confirmedColor)
					continue;
			}

			if (viewname == "gameboard")
			{
				if (!isBorder1(r, c))
				{
					if (putPiece(remapRow(r), remapCol(c), currentColor))
					{
						//1. update gameview
						ViewInfo viPut;
						viPut.color = (WORD)currentColor;
						viPut.view_start_x = c;
						viPut.view_start_y = r;
						viPut.name = "putpiece";

						controller.model.notify(viPut);

						updateScoreView();

						//3.update moveview

						 //1.how many lines
						int numlines = 10 < moves.size() ? 10 : (int)moves.size();
						//2.where to start
						int start = (int)moves.size() - 10;
						if (start < 0)
							start = 0;
						for (int i = start; i < start + numlines; i++)
						{
							ostringstream outputMove;
							//outputMove << "["<<i+1<<"]" << " player"<<get<0>(moves[i])<<" "<< get<1>(moves[i])<<","<< get<2>(moves[i]);
							outputMove << setw(2) << "[" << i + 1 + moveviewBias << "]" << (char)(get<2>(moves[i]) + 65) << "," << get<1>(moves[i]) << setw(2) << "     ";
							auto textMove = outputMove.str();

							ViewInfo viMove;
							viMove.name = "move";
							viMove.view_start_x = 0;
							viMove.view_start_y = i - start;
							viMove.text = textMove;
							if (get<0>(moves[i]) == 1)
							{
								viMove.color = 0x0010 | 0x0020 | 0x0040 | backToFore(p1Color);
							}
							else
								viMove.color = 0x0010 | 0x0020 | 0x0040 | backToFore(p2Color);

							controller.model.notify(viMove);
						}
					}
				}
			}
			else if (viewname == "nameview")
			{
				//std::cout << "name: c: " << c << ", " << "r: " << r << std::endl;

				HANDLE hConsoleInput, hConsoleOutput;
				hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
				hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
				string::size_type editControlCursorPos = 0;
				decltype(editControlCursorPos) editControlAperture = 0;
				string editControlString = c + "," + r;




				SHORT minc, minr, maxc, maxr, origc, origr = 0;

				origc = minc = +86;
				maxc = +86 + 10;
				minr = maxr = origr = +6;


				auto editControlHit = true;
				editControlHit = r == +6 && c >= minc && c <= maxc;
				// clickPos.Y == EDIT_CONTROL_START.Y && EDIT_CONTROL_START.X <= clickPos.X && clickPos.X < EDIT_CONTROL_START.X + EDIT_CONTROL_LENGTH;
				if (editControlHit) {
					//if (!editControlHasFocus)
						//UpdateStatusMessage("Edit control selected");

					// show the cursor at the selection point
					CONSOLE_CURSOR_INFO cci{ 10, TRUE };

					THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &cci));


					//这里看string 长度。噢
					COORD constexpr EDIT_CONTROL_START = COORD{ 2 + 80, 0 + 1 };
					editControlCursorPos = min(c - EDIT_CONTROL_START.X + editControlAperture, editControlString.size());



					//COORD loc{ SHORT(editControlCursorPos - editControlAperture + EDIT_CONTROL_START.X), EDIT_CONTROL_START.Y };



					//COORD loc{c, r };
					COORD loc{ origc,origr };

					THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, loc));
				}
			}
			else if (viewname == "undo" && moves.size() > 0)
			{
				undoHelper();
			}
		}
		else if (e == "MR")
		{
			//initializGame();
		}
		else if (e == "MU" || e == "MD")
		{

			if (e == "MU")
				moveviewBias -= 1;
			else
				moveviewBias += 1;

			//see if at moveview
			if (viewFocus == "moveview")
			{
				//1.how many lines
				int thenum = 10;
				int numlines = thenum < (int)moves.size() ? thenum : (int)moves.size();
				//2.where to start
				int start = (int)moves.size() - thenum;
				if (start < 0)
					start = 0;

				int min = start;
				int max = start + numlines;

				if (min + moveviewBias < 0)
					moveviewBias++;
				if (max + moveviewBias > (int)moves.size())
					moveviewBias = 0;


				for (int i = start; i < start + numlines; i++)
				{
					ostringstream outputMove;
					//outputMove << "["<<i+1<<"]" << " player"<<get<0>(moves[i])<<" "<< get<1>(moves[i])<<","<< get<2>(moves[i]);
					outputMove << setw(2) << "[" << i + 1 + moveviewBias << "]" << (char)(get<2>(moves[i + moveviewBias]) + 65) << "," << get<1>(moves[i + moveviewBias]) << setw(2) << "     ";
					auto textMove = outputMove.str();

					ViewInfo viMove;
					viMove.name = "move";
					viMove.view_start_x = 0;
					viMove.view_start_y = (SHORT)(i - start);
					viMove.text = textMove;
					if (get<0>(moves[i]) == 1)
					{
						viMove.color = 0x0010 | 0x0020 | 0x0040 | backToFore(p1Color);
					}
					else
						viMove.color = 0x0010 | 0x0020 | 0x0040 | backToFore(p2Color);

					controller.model.notify(viMove);

					//OutputString(80 + 2, 15 + 3+5 + i-start, textMove, MOUSE_SECTION_ATTR, 29); 
				}
			}
		}
		else if (e == "r")
		{
			initialize();
		}
		else if (gameover)
		{
			break;
		}
	}
	return 0;
}

int Gomoku::checkResultHelper(int &x, int &y)
{
	int count = 1;

	// horizontal
	int counth = checkDirection(x, y, "H");
	// vertical. same y, 5 continue x
	int countv = checkDirection(x, y, "V");
	// north west 
	int countnw = checkDirection(x, y, "NW");
	// north east 
	int countne = checkDirection(x, y, "NE");

	if (count < counth)
		count = counth;
	if (count < countv)
		count = countv;
	if (count < countnw)
		count = countnw;
	if (count < countne)
		count = countne;

	return count;
}

int Gomoku::checkDirection(int &x, int &y, string direction)
{
	vector<tuple<int, int, int>> * pv;
	pv = &moves;
	int player = 1;
	if (currentColor == p2Color)
		player = 1;
	else if (currentColor == p1Color)
		player = 2;

	bool moveRight = true;
	bool moveLeft = true;
	int count = 1;

	//positive increment
	int incrementX = 1;
	int incrementY = 1;

	int xDirection = 1;
	int yDirection = 1;

	if (direction == "H")
	{
		xDirection = 1;
		yDirection = 0;
	}
	else if (direction == "V")
	{
		xDirection = 0;
		yDirection = 1;
	}
	else if (direction == "NE")
	{
		xDirection = +1;
		yDirection = -1;
	}
	else if (direction == "NW")
	{
		xDirection = 1;
		yDirection = 1;
	}

	while (moveRight)
	{
		if (find(pv->begin(), pv->end(), make_tuple(player, x + incrementX * xDirection, y + incrementY * yDirection)) != pv->end())
		{
			//move up
			incrementX++;
			incrementY++;

			count++;
			continue;
		}
		else
		{
			//cout <<"R->"<< x << "," << y << "can't find " << x + incrementX * xDirection << "," << y + incrementY * yDirection << endl;
			moveRight = false;

			//negative increment
			incrementX = -1;
			incrementY = -1;

			while (moveLeft)
			{
				if (find(pv->begin(), pv->end(), make_tuple(player, x + incrementX * xDirection, y + incrementY * yDirection)) != pv->end())
				{
					//move down
					incrementX--;
					incrementY--;
					count++;
					continue;
				}
				else
				{
					moveLeft = false;
				}
			}
		}
	}
	return count;

	//if (count >= 5)
	//{
	//	gameover = true;
	//	printMsg(currentPieceColor + " Wins!", 0, 0);
	//	printMsg("Press r to play again", 0, 1);
	//	printMsg("Press any other key quit...", 0, 2);
	//	//cout << currentColor << " Wins" << endl;
	//	//cout << "Play again (r)?" << endl;

	//}

}

void Gomoku::undoHelper()
{
	ChangeCurrentColor();
	ViewInfo vi;
	vi.name = "undo";
	controller.model.notify(vi);
	moves.pop_back();
	moveviewBias = 0;
	//3.update moveview 
	for (size_t i = 0; i < 10; i++)
	{
		ViewInfo viMove;
		viMove.name = "move";
		viMove.view_start_x = 0;
		viMove.view_start_y = (SHORT)i;
		viMove.text = "          ";
		viMove.color = 0x0010 | 0x0020 | 0x0040;
		controller.model.notify(viMove);
	}

	//1.how many lines
	int numlines = 10 < (int)moves.size() ? 10 : (int)moves.size();
	//2.where to start
	int start = 0;


	for (int i = start; i < start + numlines; i++)
	{
		ostringstream outputMove;
		outputMove << setw(2) << "[" << i + 1 << "]" << (char)(get<2>(moves[i]) + 65) << "," << get<1>(moves[i]) << setw(4) << "     ";
		auto textMove = outputMove.str();

		if (textMove.size() > 13)
			textMove = "";

		ViewInfo viMove;
		viMove.name = "move";
		viMove.view_start_x = 0;
		viMove.view_start_y = (SHORT)(i - start);
		viMove.text = textMove;
		if (get<0>(moves[i]) == 1)
		{
			viMove.color = 0x0010 | 0x0020 | 0x0040 | backToFore(p1Color);
		}
		else
			viMove.color = 0x0010 | 0x0020 | 0x0040 | backToFore(p2Color);
		controller.model.notify(viMove);
	}
}

void Gomoku::updateScoreView()
{
	//2.update ScoreView 1.moves.size 2.lastCount 
	ostringstream output;


	//output << moves.size() << "   ";
	int p1num = (int)moves.size() % 2 == 0 ? (int)moves.size() / 2 : (int)moves.size() / 2 + 1;
	output << "P1-" << p1num;

	int p2num = (int)moves.size() / 2;

	output << "  P2-" << p2num<<"  ";

	auto text1 = output.str();
	ViewInfo viScore1;
	viScore1.text = text1;
	viScore1.name = "score1";

	controller.model.notify(viScore1);

	ostringstream output2;
	if (moves.size() == 1)
	{
		//if (currentColor == p2Color)
		output2 << "P1-" << "1  ";
		//else
		output2 << "P2-" << "0  ";
	}
	else if (moves.size() == 2)
	{
		//if (currentColor == p2Color)
		output2 << "P1-" << "1  ";
		//else
		output2 << "P2-" << "1  ";
	}
	else
	{
		//if (currentColor == p2Color)
		output2 << "P1-" << p1Longest << "  ";
		//else
		output2 << "P2-" << p2Longest << "   ";
	}

	auto text2 = output2.str();
	ViewInfo viScore2;
	viScore2.text = text2;
	viScore2.name = "score2";

	controller.model.notify(viScore2);
}
