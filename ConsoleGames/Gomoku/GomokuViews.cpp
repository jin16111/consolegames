﻿#include "GomokuViews.hpp"
/*!	\file		GomokuViews.cpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 GomokuView classes methods implementation
*/

bool isBorder(int r, int c)
{
	if (r % 2 == 0 || c % 4 == 0 )
		return true;
	else 
		return false;
}

GameBoardView::GameBoardView()
{
	ddcmd.app = this->app;
}

void GameBoardView::update(ViewInfo val)
{
	if (val.name == "show")
		show();
	else if (val.name == "putpiece")
	{
		if (val.view_start_x % 2 == 0)
		{
			//call command
			ddcmd.execute(val);
			val.view_start_x += 1;
			ddcmd.execute(val); 
		}
		else
		{
			//call command
			ddcmd.execute(val);
			val.view_start_x -= 1;
			ddcmd.execute(val);
		}

	}
	else if (val.name == "undo")
	{
		ddcmd.undo();
		ddcmd.undo(); 
	}
} 

void GameBoardView::show()
{  
	int yStart = 0;
	int xStart = 0; 
	
	//gameboardview scope
	///////////////////////
	int row = 42 - 3;
	int column = 38 + 4+34;
	///////////////////////

	int columnSide = 34;

	int coltotoal = column + columnSide;

	string gameBoard = string(row *coltotoal, ' ');
	 
	bool s = true; 



	vector<WORD> attr;// (row *coltotoal, 0x0010 | 0x0020 | 0x0040 | 0x0080);
	for (int r = 0; r < row; r++)
	{
		for (int c = 0; c < coltotoal; c++)
		{
			if (c <= column)
				attr.push_back(0x0010 | 0x0020 | 0x0040 | 0x0080);
			else
				attr.push_back(0);
		}
	}



	for (int r = 0; r < row; r++)
	{
		for (int c = 0; c < coltotoal; c++)
		{
			if (c <= column)
			{ 
				 
				if (isBorder(r, c))
				{
					attr[c + r * coltotoal] = (0x0010 | 0x0020 | 0x0040);
					attr[c + r * coltotoal + 1] = (0x0010 | 0x0020 | 0x0040);
				}
			}
		}
	}
 
	app->setColorString(gameBoard, attr); 
}

void GameBoardView::initialize()
{

}

string GameBoardView::isMe(int r, int c)
{
	if (c <= RANGE_COLUMN)
	{
		return "gameboard";
	}
	else
		return "";
}

void NameView::update(ViewInfo val)
{
	if (val.name == "show")
		show();
	else if (val.name == "winner")
	{
		val.view_start_x = 80;
		val.view_start_y = 12;
		val.width = 40; 
		strcmd.execute(val);
		ViewInfo v2;
		v2 = val;
		v2.text = "Press any other key to quit";
		v2.view_start_y = 12 + 1;
		strcmd.execute(v2);

	}
	else if
		(val.name == "p1color")
	{
		ViewInfo vExp;
		vExp.view_start_x = P1.X;
		vExp.view_start_y = P1.Y;
		vExp.color = val.color | 0x0001 | 0x0002 | 0x0004;
		vExp.text = "P1 Color";
		vExp.width = EDIT_SECTION_COL;
		strcmd.execute(vExp);
	}
	else if(val.name == "p2color")
	{
		ViewInfo vExp;
		vExp.view_start_x = P2.X;
		vExp.view_start_y = P2.Y;
		vExp.color = val.color | 0x0001 | 0x0002 | 0x0004;
		vExp.text = "P2 Color";
		vExp.width = EDIT_SECTION_COL;
		strcmd.execute(vExp);
	}
	else if (val.name == "confirm")
	{
		ViewInfo vExp;
		vExp.view_start_x = CONFIRM.X;
		vExp.view_start_y = CONFIRM.Y;
		vExp.color = 0x0040 | 0x0010 | 0x0020 | 0;
		vExp.text = "Started";
		vExp.width = EDIT_SECTION_COL;
		strcmd.execute(vExp);
	}
}

void NameView::show()
{  
	for (int r = 0; r < EDIT_SECTION_ROW; r++)
	{
		for (int c = 0; c < EDIT_SECTION_COL; c++)
		{
			ViewInfo v0;
			v0.view_start_x = c + START_X;
			v0.view_start_y = r + START_Y;
			v0.color = COLOR;
			v0.width = EDIT_SECTION_COL;
			dotcmd.execute(v0); 
		}
	}

	ViewInfo vExp;
	vExp.view_start_x = EXPLAIN.X-3;
	vExp.view_start_y = EXPLAIN.Y; 
	vExp.color = COLOR | 0x0001;// | 0x0002 | 0x0004;
	vExp.text = "Click To Change Color↓↓"; 
	vExp.width = EDIT_SECTION_COL;
	strcmd.execute(vExp);

	vExp.view_start_x = P1.X;
	vExp.view_start_y = P1.Y;
	vExp.color = 0x0001 | 0x0002 | 0x0004;
	vExp.text = "P1 Color";
	vExp.width = EDIT_SECTION_COL;
	strcmd.execute(vExp);
	
	vExp.view_start_x = P2.X;
	vExp.view_start_y = P2.Y;
	vExp.color = 0x0040 | 0x0001 | 0x0002 | 0x0004;
	vExp.text = "P2 Color";
	vExp.width = EDIT_SECTION_COL;
	strcmd.execute(vExp);

	vExp.view_start_x = CONFIRM.X;
	vExp.view_start_y = CONFIRM.Y;
	vExp.color =  0x0004 | 0x0001 | 0x0002 | 0;
	vExp.text = "START";
	vExp.width = EDIT_SECTION_COL;
	strcmd.execute(vExp);


	//ViewInfo v1;
	//v1.startCoord = EDIT_CONTROL_START;
	//v1.color = EDIT_CONTROL_ATTR;
	//v1.width = EDIT_CONTROL_LENGTH; 
	//editccmd.execute(v1); 
}

void NameView::initialize()
{

}

string NameView::isMe(int r, int c)
{
	if (r == 6 && c >= 99 && c <= 105)
		return "confirm";
	if (r == 4) 
		if (c >= 85 && c <= 91)
			return "p1color";
		else if (c >= 95 && c <= 101) 
			return "p2color";  
	if (r < EDIT_SECTION_ROW  && c >= 80 && r > START_Y)
		return "nameview";
	else
		return "";
}

ScoreView::ScoreView()
{
	strcmd.app = app;
}

void ScoreView::update(ViewInfo val)
{
	if (val.name == "show")
		show();
	else if (val.name == "score1")
	{
		val.color = (WORD)SCORE_COLOR;
		val.view_start_x = 80 + 2 + 14 -1;
		val.view_start_y = START_Y + 1;
		val.width = SECTION_COL;
		strcmd.execute(val);
	}

	else if (val.name == "score2")
	{
		val.color = (WORD)SCORE_COLOR;
		val.view_start_x = 80 + 2 + 14;
		val.view_start_y = START_Y + 3;
		val.width = SECTION_COL;
		strcmd.execute(val);
	}
	else if (val.name == "undo")
	{
		strcmd.undo2(); 
	}
}

void ScoreView::show()
{
	for (int r = 0; r < SECTION_ROW; r++)
	{
		for (int c = 0; c < SECTION_COL; c++)
		{
			ViewInfo v0;
			v0.view_start_x = c + START_X;
			v0.view_start_y = r + START_Y; 
			v0.color = (WORD)SCORE_COLOR;
			v0.width = SECTION_COL;
			dotcmd.execute(v0); 
		}
	} 
	ViewInfo v;

	ostringstream output;
	output << "Total Moves: " << "P1-0  P2-0   ";
	auto text = output.str();


	v.view_start_x = 82;
	v.view_start_y = 16;
	v.text = text;
	v.color = (WORD)SCORE_COLOR;
	v.width = SECTION_COL;
	strcmd.execute(v); 

	ostringstream output2;
	output2 << "Longest  Run: " << "P1-0 P2-0   ";
	auto text2 = output2.str();

	
	v.view_start_x = 82;
	v.view_start_y = 18;
	v.text = text2;
	v.color = (WORD)SCORE_COLOR;
	v.width = SECTION_COL;
	strcmd.execute(v); 
}

void ScoreView::initialize()
{
}

string ScoreView::isMe(int r, int c)
{
	if (c >= 80 && (r > 15 -1 && r < 15 + SECTION_ROW))
		return "scoreview";
	else
		return "";
}

void MoveView::update(ViewInfo val)
{
	if (val.name == "show")
		show(); 
	if (val.name == "move")
	{
		val.view_start_x += START_X + 2;
		val.view_start_y += START_Y + 3;
		//val.color = COLOR | val.color;
		strcmd.execute(val);
	}
}

void MoveView::show()
{
	for (int r = 0; r < SECTION_ROW; r++)
	{
		for (int c = 0; c < SECTION_COL; c++)
		{

			ViewInfo v0;
			v0.view_start_x = c + START_X;
			v0.view_start_y = r + START_Y;
			v0.color = COLOR;
			v0.width = SECTION_COL;
			dotcmd.execute(v0);
			//app->setCellBackGroundColor(COLOR_STR, c + START_X, r + START_Y);
		}
	}


	ViewInfo v;

	ostringstream output;
	output << "CLICK TO UNDO";
	auto text = output.str();
	 

	v.view_start_x = BUTTON_START_X - 16;
	v.view_start_y = BUTTON_START_Y - 1;
	v.text = "Scrollable↓";
	v.color = 0x0002;
	v.width = SECTION_COL;
	strcmd.execute(v);

	//for (int i = 0; i < 3; i++)
	//{
		ViewInfo v0;
		v0.view_start_x = BUTTON_START_X;
		v0.view_start_y = BUTTON_START_Y;
		v0.color = 0x0002|0x0010;
		v0.text = " Undo ";
		v0.width = SECTION_COL;
		strcmd.execute(v0);
	//}

}

void MoveView::initialize()
{
}

string MoveView::isMe(int r, int c)
{
	if (c >= 97 && c <= 102 && r == 22)
		return "undo";
	if (c >= 80 && (r >= 20 && r <=37 ))//&& r < 15 + EDIT_SECTION_ROW))
		return "moveview";
	else
		return "";
}
