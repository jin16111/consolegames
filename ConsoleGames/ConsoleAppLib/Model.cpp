//CPP File
#include "Model.hpp"
#include <algorithm>

/*!	\file		Model.cpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 implement Model class's methods
*/

using namespace std;

void Model::attach(IObserver * View)
{
	list.push_back(View);
}
void Model::detach(IObserver * View)
{
	list.erase(std::remove(list.begin(), list.end(), View), list.end());
}

void Model::notify(ViewInfo value)
{
	for (vector<IObserver *>::const_iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		if (*iter != 0)
		{ 
			(*iter)->update(value);
		}
	}
}

string Model::query(int r, int c)
{
	string name = "";
	for (vector<IObserver *>::const_iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		if (*iter != 0)
		{
			name = (*iter)->isMe(r,c);
			if (name != "")
				return name;
		}
	}
	return "";
}



