#include "ConsoleController.hpp"
/*!	\file		ConsoleController.cpp
	\author		Jinzhi Yang
	\date		2019-03-01

	ConsoleController class methods implementation.
	ConsoleController is the inner facade which monitors and interacts with Win32 Console applicaion.
*/


bool* ConsoleController::done;
unique_ptr<ConsoleController> ConsoleController::_instance;

ConsoleController& ConsoleController::instance()
{
	if (!_instance)
		_instance = make_unique<ConsoleController>();
	return *_instance;
}

ConsoleController::ConsoleController()
{
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);

	DWORD consoleMode = /*ENABLE_WINDOW_INPUT |*/ ENABLE_PROCESSED_INPUT | ENABLE_MOUSE_INPUT;
	consoleMode |= ENABLE_EXTENDED_FLAGS;	// Stop windows from taking over the mouse.

	// Install a control handler to trap ^C
	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE))
	{
		cerr << "ERROR: failed to install control handler." << endl;
	}
}

void ConsoleController::setFlag(bool* d) {
	done = d;
}

tuple<char, short, short>  ConsoleController::mouseEventProc3(MOUSE_EVENT_RECORD const& mer) {
#if !defined(MOUSE_HWHEELED)
#define MOUSE_HWHEELED 0x0008
#endif
	//cout << "Mouse event:";
	auto bufferLoc = mer.dwMousePosition;
	switch (mer.dwEventFlags)
	{
		case 0:	// button pressed or released
			{
				auto mask = mer.dwButtonState;
				if (mask&FROM_LEFT_1ST_BUTTON_PRESSED)
				{

					return make_tuple('L', bufferLoc.X, bufferLoc.Y);
				}
				else if (mask&RIGHTMOST_BUTTON_PRESSED)
				{
					return make_tuple('R', bufferLoc.X, bufferLoc.Y);
				}
			}
		break;
		case MOUSE_WHEELED: 
			{
			short state = ((WORD)((((DWORD_PTR)((mer.dwButtonState))) >> 16) & 0xffff));
			if(state > 0)
				return make_tuple('U', bufferLoc.X, bufferLoc.Y);
			else
				return make_tuple('D', bufferLoc.X, bufferLoc.Y); 
			}
							break;
	}
	return make_tuple(' ', bufferLoc.X, bufferLoc.Y);
}

tuple<string, short, short>  ConsoleController::mouseEventProc0(MOUSE_EVENT_RECORD const& mer) {
#if !defined(MOUSE_HWHEELED)
#define MOUSE_HWHEELED 0x0008
#endif
	//cout << "Mouse event:";
	auto bufferLoc = mer.dwMousePosition;
	switch (mer.dwEventFlags)
	{
	case 0:	// button pressed or released
	{
		auto mask = mer.dwButtonState;
		if (mask&FROM_LEFT_1ST_BUTTON_PRESSED)
		{

			return make_tuple("ML", bufferLoc.X, bufferLoc.Y);
		}
		else if (mask&RIGHTMOST_BUTTON_PRESSED)
		{
			return make_tuple("MR", bufferLoc.X, bufferLoc.Y);
		}
	}
	break;
	case MOUSE_WHEELED:
	{
		short state = ((WORD)((((DWORD_PTR)((mer.dwButtonState))) >> 16) & 0xffff));
		if (state > 0)
			return make_tuple("MU", bufferLoc.X, bufferLoc.Y);
		else
			return make_tuple("MD", bufferLoc.X, bufferLoc.Y);
	}
	break;
	}
	return make_tuple(" ", bufferLoc.X, bufferLoc.Y);
}

tuple<char, short, short> ConsoleController::startMonitoringInput3()
{
	vector<INPUT_RECORD> inBuffer(128);
	while (true)
	{
		DWORD numEvents;
		if (!ReadConsoleInput(hConsoleInput, inBuffer.data(), (DWORD)inBuffer.size(), &numEvents)) {
			cerr << "Failed to read console input\n";
			break;
		}

		for (size_t iEvent = 0; iEvent < numEvents; ++iEvent) {
			switch (inBuffer[iEvent].EventType) {

			case MOUSE_EVENT:
				return mouseEventProc3(inBuffer[iEvent].Event.MouseEvent);
				break;
			case KEY_EVENT:
				//if(inBuffer[iEvent].Event.KeyEvent.bKeyDown)
				return make_tuple(inBuffer[iEvent].Event.KeyEvent.uChar.AsciiChar, 0, 0);
				break;
			}
		}
	}
	return make_tuple(' ', 0, 0);
}

tuple<string, short, short> ConsoleController::startMonitoringInput0()
{
	vector<INPUT_RECORD> inBuffer(128);
	while (true)
	{
		DWORD numEvents;
		if (!ReadConsoleInput(hConsoleInput, inBuffer.data(), (DWORD)inBuffer.size(), &numEvents)) {
			cerr << "Failed to read console input\n";
			break;
		}

		for (size_t iEvent = 0; iEvent < numEvents; ++iEvent) {
			switch (inBuffer[iEvent].EventType) {

			case MOUSE_EVENT:
				return mouseEventProc0(inBuffer[iEvent].Event.MouseEvent);
				break;
			case KEY_EVENT:
				//if(inBuffer[iEvent].Event.KeyEvent.bKeyDown)
				return make_tuple(string (1,inBuffer[iEvent].Event.KeyEvent.uChar.AsciiChar), 0, 0);
				break;
			}
		}
	}
	return make_tuple(" ", 0, 0);
}

void ConsoleController::setCellBackGroundColor(string Color, short x, short y)
{
	// Get the number of character cells in the current buffer
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

	// Fill the entire screen area white
	//DWORD charsWritten;
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	COORD cursorHomeCoord{ 0, 0 };

	// Print message in the middle of the screen
	string msg = " ";

	unsigned short bgColor = BACKGROUND_RED;

	if (Color == "BLUE")
		bgColor = BACKGROUND_BLUE;
	else if (Color == "GREEN")
		bgColor = BACKGROUND_GREEN;
	else if (Color == "BLACK")
		bgColor = BACKGROUND_BLACK;
	else if (Color == "WHITE")
		bgColor = BACKGROUND_WHITE;

	vector<WORD> attr{
		bgColor
	};

	COORD loc;
	loc.X = x;
	loc.Y = y;
	DWORD nCharsWritten;
	WriteConsoleOutputCharacterA(hConsoleOutput, msg.c_str(), (DWORD)msg.size(), loc, &nCharsWritten);
	WriteConsoleOutputAttribute(hConsoleOutput, attr.data(), (DWORD)attr.size(), loc, &nCharsWritten);
}

void ConsoleController::setCellBackGroundColor(DWORD Color, short x, short y)
{
	// Get the number of character cells in the current buffer
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

	// Fill the entire screen area white
	//DWORD charsWritten;
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	COORD cursorHomeCoord{ 0, 0 };

	// Print message in the middle of the screen
	string msg = " ";

	unsigned short bgColor = (unsigned short)Color;

	vector<WORD> attr{
		bgColor
	};

	COORD loc;
	loc.X = x;
	loc.Y = y;
	DWORD nCharsWritten;
	WriteConsoleOutputCharacterA(hConsoleOutput, msg.c_str(), (DWORD)msg.size(), loc, &nCharsWritten);
	WriteConsoleOutputAttribute(hConsoleOutput, attr.data(), (DWORD)attr.size(), loc, &nCharsWritten);
}

void ConsoleController::setBackGroundColor(WORD color, string area)
{
	// Get the number of character cells in the current buffer
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

	// Fill the entire screen area white
	DWORD charsWritten;
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	COORD cursorHomeCoord{ 0, 0 };
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputCharacterA(hConsoleOutput, ' ', consoleSize, cursorHomeCoord, &charsWritten));
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, color, consoleSize, cursorHomeCoord, &charsWritten));

}

void ConsoleController::drawView(COORD cursorHomeCoord, DWORD consoleSize, WORD view1Color) {
	DWORD charsWritten;
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputCharacterA(hConsoleOutput, ' ', consoleSize, cursorHomeCoord, &charsWritten));
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, view1Color, consoleSize, cursorHomeCoord, &charsWritten));
}

void ConsoleController::setBackGroundColorWhite()
{
	// Get the number of character cells in the current buffer
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

	// Fill the entire screen area white
	DWORD charsWritten;
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	COORD cursorHomeCoord{ 0, 0 };
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputCharacterA(hConsoleOutput, ' ', consoleSize, cursorHomeCoord, &charsWritten));
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, BACKGROUND_WHITE | BACKGROUND_INTENSITY, consoleSize, cursorHomeCoord, &charsWritten));

}

void ConsoleController::setBackGroundColorWithChar(string& s)
{
	// Get the number of character cells in the current buffer
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

	// Fill the entire screen area white
	//DWORD charsWritten;
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	COORD cursorHomeCoord{ 0, 0 };


	vector<WORD> attr;
	for (size_t i = 0; i < s.length(); i++)
	{
		attr.push_back(FOREGROUND_BLACK | BACKGROUND_WHITE);
	}

	COORD loc;
	loc.X = 0;
	loc.Y = 0;
	DWORD nCharsWritten;
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), (DWORD)s.size(), loc, &nCharsWritten));
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputAttribute(hConsoleOutput, attr.data(), (DWORD)attr.size(), loc, &nCharsWritten));

}

void ConsoleController::setColorString(string& s, vector<WORD> attr)
{
	// Get the number of character cells in the current buffer
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

	// Fill the entire screen area white
	//DWORD charsWritten;
	DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
	COORD cursorHomeCoord{ 0, 0 };



	COORD loc;
	loc.X = 0;
	loc.Y = 0;
	DWORD nCharsWritten;
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), (DWORD)s.size(), loc, &nCharsWritten));
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputAttribute(hConsoleOutput, attr.data(), (DWORD)attr.size(), loc, &nCharsWritten));
}



void ConsoleController::printMsg(string& msg, short x, short y)
{
	vector<WORD> attr;
	for (size_t i = 0; i < msg.length(); i++)
	{
		attr.push_back(FOREGROUND_GREEN | BACKGROUND_BLACK);
	}

	COORD loc;
	loc.X = x;
	loc.Y = y;
	DWORD nCharsWritten;
	WriteConsoleOutputCharacterA(hConsoleOutput, msg.c_str(), (DWORD)msg.size(), loc, &nCharsWritten);
	WriteConsoleOutputAttribute(hConsoleOutput, attr.data(), (DWORD)attr.size(), loc, &nCharsWritten);
}

void ConsoleController::setTitle(LPCSTR title)
{
	SetConsoleTitleA(title);
}

BOOL ConsoleController::CtrlHandler(DWORD ctrlType) {
	switch (ctrlType) {
	case CTRL_C_EVENT:
		cout << "Ctrl-C pressed\n" << endl;
		*done = true;
		return TRUE;
	}
	return FALSE;
}



void ConsoleController::OutputString(WORD x, WORD y, std::string const& s, WORD attribute, WORD currentConsoleWidth) {

	auto attributes = vector<WORD>(s.size(), attribute);
	COORD loc{ (SHORT)x, (SHORT)y };
	DWORD nCharsWritten;
	DWORD nToWrite = DWORD(min(s.size(), std::size_t(currentConsoleWidth - x)));

	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), nToWrite, loc, &nCharsWritten));
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputAttribute(hConsoleOutput, attributes.data(), nToWrite, loc, &nCharsWritten));
}

void ConsoleController::OutputString(WORD x, WORD y, std::string const& s, vector<WORD> const& attributes, WORD currentConsoleWidth) {
	COORD loc{ (SHORT)x, (SHORT)y };
	DWORD nCharsWritten;
	DWORD nToWrite = DWORD(min(s.size(), std::size_t(currentConsoleWidth - x)));

	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), nToWrite, loc, &nCharsWritten));
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputAttribute(hConsoleOutput, attributes.data(), nToWrite, loc, &nCharsWritten));
}

void ConsoleController::DrawEditControl(COORD cursorHomeCoord, DWORD consoleSize, WORD EDIT_CONTROL_ATTR)
{
	DWORD charsWritten;
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, EDIT_CONTROL_ATTR, consoleSize, cursorHomeCoord, &charsWritten));
}

//
//void ConsoleController::OutputString1(WORD x, WORD y, std::string const& s, vector<WORD> const& attributes) {
//
//	HANDLE hConsoleInput, hConsoleOutput;
//
//	COORD loc{ (SHORT)x, (SHORT)y };
//	DWORD nCharsWritten;
//	DWORD nToWrite = DWORD(min(s.size(), std::size_t(29 - x)));
//
//	(WriteConsoleOutputCharacterA(hConsoleOutput, s.c_str(), nToWrite, loc, &nCharsWritten));
//	(WriteConsoleOutputAttribute(hConsoleOutput, attributes.data(), nToWrite, loc, &nCharsWritten));
//}
//void ConsoleController::OutputString1(WORD x, WORD y, std::string const& s, WORD attributes) {
//	OutputString1(x, y, s, vector<WORD>(s.size(), attributes));
//}