/*!	\file		ConsoleController.cpp
	\author		Jinzhi Yang
	\date		2019-03-01

	ConsoleApplication class methods implementation.
*/


#include "ConsoleApplication.hpp"  
#include <chrono>
#include <thread>

/*!	Process entry point.
	Calls the ConsoleApplication singleton's wmain.
*/
int wmain(int argc, wchar_t* argv[]) try {
#ifdef _DEBUG
	// Enable CRT memory leak checking.
	int dbgFlags = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	dbgFlags |= _CRTDBG_CHECK_ALWAYS_DF;
	dbgFlags |= _CRTDBG_DELAY_FREE_MEM_DF;
	dbgFlags |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(dbgFlags);
#endif
	return ConsoleApplication::thisApp_sm->wmain(argc, argv);
}
catch (char const * msg) {
	wcerr << L"exception string: " << msg << endl;
}
catch (exception const& e) {
	wcerr << L"std::exception: " << e.what() << endl;
}
catch (...) {
	wcerr << L"Error: an exception has been caught...\n";
	return EXIT_FAILURE;
}



/*!	ConsoleApplication singleton instance pointer.
*/
ConsoleApplication* ConsoleApplication::thisApp_sm = nullptr;



/*!	wmain configures the application.
*/
int ConsoleApplication::wmain(int argc, wchar_t* argv[]) {
	return execute();
}






/*!	execute the application.
	Override this method in the derived class.
*/
int ConsoleApplication::execute() {
	wcout << "Console application framework: (c) 2019, Garth Santor\n";
	return EXIT_SUCCESS;
}


///*!	Singleton initialization and confirmation.
ConsoleApplication::ConsoleApplication() {
	if (thisApp_sm)
		throw std::logic_error("Error: ConsoleApplication already initialized!");
	thisApp_sm = this;

	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);

	saveConsoleState();

	DWORD consoleMode = /*ENABLE_WINDOW_INPUT |*/ ENABLE_PROCESSED_INPUT | ENABLE_MOUSE_INPUT;
	consoleMode |= ENABLE_EXTENDED_FLAGS;	// Stop windows from taking over the mouse.

	if (!SetConsoleMode(hConsoleInput, consoleMode)) {
		cerr << "\nERROR: could not set console mode.\n";
	}
}
ConsoleApplication::~ConsoleApplication()
{
	restoreWindow();
}

void ConsoleApplication::saveConsoleState()
{
#pragma region SaveTitle
	originalTitle.resize(64 * 1024);
	originalTitle.resize(size_t(GetConsoleTitleA(originalTitle.data(), (DWORD)originalTitle.size())) + 1);
	originalTitle.shrink_to_fit();
#pragma endregion

#pragma region SaveConsoleState
	// Get the old window/buffer size
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &originalCSBI));

	// Save the desktop
	originalBuffer.resize(size_t(originalCSBI.dwSize.X)*originalCSBI.dwSize.Y);
	originalBufferCoord = COORD{ 0 };
	SMALL_RECT bufferRect{ 0 };
	bufferRect.Right = originalCSBI.dwSize.X - 1;
	bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
	THROW_IF_CONSOLE_ERROR(ReadConsoleOutputA(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, &bufferRect));

	// Save the cursor
	THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(hConsoleOutput, &originalCCI_));

	//Save ConsoleMode
	THROW_IF_CONSOLE_ERROR(GetConsoleMode(hConsoleInput, &originalConsoleMode));
#pragma endregion


}

void ConsoleApplication::resizeWindow(WORD WINDOW_WIDTH, WORD WINDOW_HEIGHT)
{
#pragma region ResizeWindow
	{
		SMALL_RECT sr{ 0 };
		THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));

		COORD bufferSize;
		bufferSize.X = WINDOW_WIDTH;
		bufferSize.Y = WINDOW_HEIGHT;
		THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, bufferSize));

		CONSOLE_SCREEN_BUFFER_INFO sbi;
		THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &sbi));

		sr.Top = sr.Left = 0;
		WINDOW_WIDTH = std::min((SHORT)WINDOW_WIDTH, sbi.dwMaximumWindowSize.X);
		WINDOW_HEIGHT = std::min((SHORT)WINDOW_HEIGHT, sbi.dwMaximumWindowSize.Y);
		sr.Right = WINDOW_WIDTH - 1;
		sr.Bottom = WINDOW_HEIGHT - 1;

		THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));
		currentConsoleWidth_ = sr.Right - sr.Left + 1;



	}
#pragma endregion

#pragma region HideTheCursor
	{
		auto newCCI = originalCCI_;
		newCCI.bVisible = FALSE;
		THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &newCCI));
	}
#pragma endregion
}

void ConsoleApplication::restoreWindow()
{
#pragma region RestoreWindow
	{
		SMALL_RECT bufferRect{ 0 };

		// Restore the original settings/size
		SMALL_RECT sr{ 0 };
		THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));
		THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, originalCSBI.dwSize));
		THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &originalCSBI.srWindow));


		// Restore the desktop contents
		bufferRect = SMALL_RECT{ 0 };
		bufferRect.Right = originalCSBI.dwSize.X - 1;
		bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
		THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, &bufferRect));
		SetConsoleTitleA(originalTitle.data());

		// Restore the cursor
		THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &originalCCI_));
		THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, originalCSBI.dwCursorPosition));

		// Restore Console Mode
		THROW_IF_CONSOLE_ERROR(SetConsoleMode(hConsoleInput, originalConsoleMode));
	}
#pragma endregion
}
