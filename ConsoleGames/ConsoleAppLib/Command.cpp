#include"Command.hpp"
#include <utility>      // std::pair

/*!	\file		Command.cpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 implementation of Command classes
*/


DrawAreaCommand::DrawAreaCommand()
{
	
}

DrawAreaCommand::DrawAreaCommand(ConsoleApplication* newd)
{
	app = newd;
}

void DrawAreaCommand::execute(ViewInfo vi)
{
	app->drawView(vi.startCoord, vi.width * vi.height, vi.color);
}


DrawDotCommand::DrawDotCommand(ConsoleApplication* newd)
{
	app = newd;
}

void DrawDotCommand::execute(ViewInfo vi)
{
	if (vi.width == 0)
		vi.width = 40;
	app->OutputString(vi.view_start_x,vi.view_start_y," ",vi.color, vi.width );

	auto p = std::make_pair(vi.view_start_x, vi.view_start_y);
	history.push_back(p); 
}

void DrawDotCommand::undo()
{
	auto p = history[history.size() - 1];
	app->OutputString(p.first, p.second, " ", BACKGROUND_INTENSITY|0x0010|0x0020|0x0040, 40);
	history.pop_back();
}


CommandV2Obsolete::CommandV2Obsolete(ConsoleApplication *obj, void(ConsoleApplication:: *meth)())
{
	object = obj; // the argument's name is "meth"
	method = meth;
}
void CommandV2Obsolete::execute()
{
	(object->*method)(); // invoke the method on the object
}

void OutputStrCommand::execute(ViewInfo vi)
{
	history.push_back(vi);
	app->OutputString(vi.view_start_x, vi.view_start_y, vi.text, vi.color, vi.width);
}

void OutputStrCommand::undo()
{
	auto vi = history[history.size() - 1];
	app->OutputString(vi.view_start_x, vi.view_start_y, vi.text, vi.color, vi.width); 
	history.pop_back();
}

void OutputStrCommand::undo2()
{
	history.pop_back(); 
	history.pop_back();
	auto vi = history[history.size() - 1];
	app->OutputString(vi.view_start_x, vi.view_start_y, vi.text, vi.color, vi.width);
	 vi = history[history.size() - 2];
	app->OutputString(vi.view_start_x, vi.view_start_y, vi.text, vi.color, vi.width);
	
}

DrawEditControlCommand::DrawEditControlCommand(ConsoleApplication * newd)
{
	
}

void DrawEditControlCommand::execute(ViewInfo vi)
{ 
	app->DrawEditControl(vi.startCoord,vi.width,vi.color);
}
