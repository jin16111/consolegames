#pragma once
#include "Model.hpp"
/*!	\file		PaintModel.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 PaintModel cantian a list of view. it is the subject of oberver patterns
*/
class PaintModel : public Model
{
public:
	LPCSTR title = "Paint";
	WORD windowWith = 40;
	WORD windowHeight = 43;
	//WORD canvasColor = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
	ViewInfo paletteInfo;
	ViewInfo canvasInfo;
	PaintModel();
	void broadcast(ViewInfo value);
};

