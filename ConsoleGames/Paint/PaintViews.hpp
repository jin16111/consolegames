#pragma once
#include "View.hpp"
/*!	\file		PaintViews.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 Contains the View classes Gomoku game. They are the oberver in observer pattern
*/
class PaletteView : public IObserver
{
	//Name of the View
	std::string name;
	ViewInfo value;
	DrawAreaCommand drawCommand;

	vector<COORD> buttons;
	vector<WORD> buttoncolors;

	ConsoleApplication* appPtr;

public:
	PaletteView(std::string n, ConsoleApplication* app);
	void update(ViewInfo value);
	void show();
	void initialize();
	string isMe(int x, int y);
};

class CanvasView : public IObserver
{
	//Name of the View
	std::string name;
	ViewInfo value;
	DrawDotCommand drawCommand;
	DrawAreaCommand drawareaCommand;
public:
	CanvasView(std::string n, ConsoleApplication* app);
	void update(ViewInfo value);
	void show();
	void initialize();
	string isMe(int x, int y);
};

