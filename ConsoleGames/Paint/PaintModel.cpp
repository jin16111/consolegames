#include "PaintModel.hpp"
/*!	\file		PaintModel.cpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 PaintModel class methods implementation
*/


PaintModel::PaintModel()
{
	paletteInfo.name = "pv";
	paletteInfo.view_start_y = 0;
	paletteInfo.view_start_x = 0;
	paletteInfo.startCoord = COORD{ 0, 0 };
	paletteInfo.height = 3;
	paletteInfo.color = 0;
	paletteInfo.width = 40;

	canvasInfo.name = "cv";
	canvasInfo.view_start_y = 3;
	canvasInfo.view_start_x = 0;
	canvasInfo.startCoord = COORD{ 0, 3 };
	canvasInfo.height = 40;
	canvasInfo.color = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
	canvasInfo.width = 40;

}

void PaintModel::broadcast(ViewInfo value)
{
	notify(value);
}
