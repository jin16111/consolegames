
/*!	\file		Paint.cpp
	\author		Jinzhi Yang
	\date		2019-03-01

	Paint class methods implementation. 
*/

#pragma once
#include "Paint.hpp"
#include "ViewInfo.hpp"
#include "PaintViews.hpp"

/*!	\file		Paint.cpp
	\author		Jinzhi Yang
	\date		2019-03-01

	Paint class methods implementation.
*/

Paint::Paint()
{
	exit = false;
	currentColor = BACKGROUND_RED;
	setFlag(&controller.exit);
}


void Paint::ChangeCurrentColor()
{
	if (helper == BACKGROUND_RED)
		helper = BACKGROUND_GREEN;
	else if (helper == BACKGROUND_GREEN)
		helper = BACKGROUND_BLUE;
	else if (helper == BACKGROUND_BLUE)
		helper = BACKGROUND_RED;
	else
		helper = BACKGROUND_RED;
}

int Paint::execute() {

	setTitle(controller.getTitle());
	resizeWindow(controller.getWidth(), controller.getHeight());

	IObserver* pv = new PaletteView("pv", this);
	controller.model.attach(pv);


	IObserver* cv = new CanvasView("cv", this);
	controller.model.attach(cv);

	controller.model.broadcast(controller.model.canvasInfo);
	controller.model.broadcast(controller.model.paletteInfo);

	pv->initialize();
	cv->initialize();
	while (!controller.exit)
	{
		auto inputEvent = startMonitoringInput3();

		if (get<0>(inputEvent) == 'L')
		{
			//leftclick, ask controller
			// if the click in canvas one area then 
			if (controller.model.canvasInfo.view_start_y <= get<2>(inputEvent) && get<2>(inputEvent) < controller.model.canvasInfo.view_start_y + controller.model.canvasInfo.height
				&&
				controller.model.canvasInfo.view_start_x <= get<1>(inputEvent) && get<1>(inputEvent) < controller.model.canvasInfo.view_start_x + controller.model.canvasInfo.width)
			{
				ViewInfo vi;
				vi.view_start_x = get<1>(inputEvent);
				vi.view_start_y = get<2>(inputEvent);
				vi.width = 40;
				vi.name = "cv";
				vi.color = helper;

				controller.model.broadcast(vi);
				//setCellBackGroundColor(currentColor, get<1>(inputEvent), get<2>(inputEvent));
			}
			else // if in pellate area
			{
				//pick the color. color information is in the view. view->getColorByCoord? sure
				ViewInfo vi;
				vi.name = "pv";
				vi.view_start_x = get<1>(inputEvent);
				vi.view_start_y = get<2>(inputEvent);
				controller.model.broadcast(vi);
			}

		}
		else if (get<0>(inputEvent) == 'R')
		{
			ChangeCurrentColor();
		}
		else if (get<0>(inputEvent) == 'c')
		{
			setBackGroundColorWhite();
		}
		get<0>(inputEvent) = ' ';

	}


	delete pv, cv;
	return EXIT_SUCCESS;
}