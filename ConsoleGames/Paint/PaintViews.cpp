
#include "PaintViews.hpp"
/*!	\file		PaintViews.cpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 PaintViews class methods implementation
*/


string PaletteView::isMe(int x, int y)
{
	return "";
}
PaletteView::PaletteView(std::string name, ConsoleApplication* app)
{
	this->name = name;
	this->drawCommand.app = app;

	appPtr = app;

	for (SHORT i = 0; i <= 36; i += 6)
	{
		if (i != 0)
			buttons.push_back(COORD{ 0 + i - 2,1 });
	}

	buttoncolors.push_back(BACKGROUND_RED);
	buttoncolors.push_back(BACKGROUND_GREEN);
	buttoncolors.push_back(BACKGROUND_BLUE);
	buttoncolors.push_back(BACKGROUND_RED | BACKGROUND_BLUE);
	buttoncolors.push_back(BACKGROUND_GREEN | BACKGROUND_BLUE);
	buttoncolors.push_back(BACKGROUND_RED | BACKGROUND_GREEN);

}

void PaletteView::update(ViewInfo value)
{
	if (value.name == name)
	{
		this->value = value;
		for (size_t i = 0; i < buttons.size(); i++) 
		{
			if (buttons[i].X == value.view_start_x && buttons[i].Y == value.view_start_y)// if value .x and .y in my list
			{ 
				appPtr->helper = buttoncolors[i];
				break;
			}
		}
	}
}

void PaletteView::show() {

}

void PaletteView::initialize() {

	drawCommand.execute(value);
	for (size_t i = 0; i < buttons.size(); i++)
	{
		appPtr->OutputString(buttons[i].X, buttons[i].Y, " ", buttoncolors[i], 40);
	}
}


string CanvasView::isMe(int x, int y)
{
	return "";
}
CanvasView::CanvasView(std::string name, ConsoleApplication* app)
{
	this->name = name;
	this->drawCommand.app = app;
	this->drawareaCommand.app = app;
}

void CanvasView::initialize() {

	value.name = "cv";
	value.view_start_y = 0;
	value.view_start_x = 3;
	value.startCoord = COORD{ 0, 3 };
	value.height = 40;
	value.color = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
	value.width = 40;

	drawareaCommand.execute(value);
}


void CanvasView::update(ViewInfo value)
{
	if (value.name == name)
	{
		this->value = value;
		show();
	}
}

void CanvasView::show() {
	drawCommand.execute(value);
}
