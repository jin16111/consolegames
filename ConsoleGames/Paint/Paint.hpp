/*!	\file		Paint.cpp
	\author		Jinzhi Yang
	\date		2019-03-01

	Paint class declaration.Paint allows user to draw a colorful cell with left click and cycles draw color with right click
*/


#include "ConsoleApplication.hpp"
#include "PaintController.hpp"

using namespace std;

class Paint : public ConsoleApplication {
	PaintController controller;
	int currentColor;
	bool exit;
	void ChangeCurrentColor();
	int execute() override;
public:
	Paint();
} paint;