#pragma once
#include "Controller.hpp"
#include"PaintModel.hpp"
/*!	\file		PaintController.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 PaintController is the controller of this MVC app. it is responsible for the communication between model and view
*/


class PaintController 
{
public:

	PaintModel model;


	bool exit = false;

	PaintController()
	{
	}

	~PaintController()
	{
	}


	inline LPCSTR getTitle()
	{
		return model.title;
	}
	inline WORD getWidth()
	{
		return model.windowWith;
	}
	inline WORD getHeight()
	{
		return model.windowHeight;
	}





};