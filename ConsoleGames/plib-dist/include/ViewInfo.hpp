#pragma once
/*!	\file		ViewInfo.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 ViewInfo contains the information sent to view's command
*/

#include <string>
#include <Windows.h>

//viewinfo contains the information that observer needs
class ViewInfo
{
public:
	std::string name;
	SHORT  view_start_y;
	SHORT  view_start_x;
	COORD startCoord;
	WORD  height;
	WORD  width;
	WORD  color; 
	std::string text;
};