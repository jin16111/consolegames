#pragma once
/*!	\file		XError.hpp
	\author		Jinzhi Yang
	\date		2019-03-01

	XError class declaration. XError contains comprehensive message about an error
*/


#include <Windows.h>
#undef min

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string> 
using namespace std;



//=======================================================
// Error exception class and utilities
//=======================================================
#pragma region XError




/* Console error exception class. */
class XError {
public:
	using id_type = decltype(GetLastError());
	using file_type = char const *;
	using string_type = std::string;
private:
	id_type code_;
	int	line_;
	file_type file_;  
	/* ErrorDescription */
	static string ErrorDescription(DWORD dwMessageID);
public: 
	XError(int line, file_type file) : code_(GetLastError()), line_(line), file_(file) {}
	auto code() const -> id_type { return code_; }
	auto line() const -> int { return line_; }
	auto file() const -> file_type { return file_; }

	string_type msg() const;
};
/* Console error exception throw helper macro. */
#define THROW_IF_CONSOLE_ERROR(res) if(!res) throw XError(__LINE__,__FILE__)
#define THROW_CONSOLE_ERROR() throw XError(__LINE__,__FILE__)

#pragma endregion
 
