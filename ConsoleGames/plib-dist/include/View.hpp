#pragma once
/*!	\file		IObserver.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 IObserver is the parent of oberver classes in observer pattern
*/

// Remaining includes 
#include <vector>
#include <Windows.h>
#include <iostream>
#include <string>
#include <tuple> 
#include "ViewInfo.hpp"
#include "Command.hpp"
using namespace std;

class IObserver
{
public:
	//update observer using ViweInfo
	virtual void update(ViewInfo value) = 0;

	//render observer
	virtual void show() = 0;

	//initialize observer
	virtual void initialize() = 0;
	
	//return if it is me by x and y
	virtual string isMe(int x, int y) = 0;
};


