#pragma once
/*!	\file		ConsoleApplication.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	ConsoleApplication class declaration. ConsoleApplication is the outer facade.
*/
#pragma comment(linker, "/SUBSYSTEM:CONSOLE /ENTRY:wmainCRTStartup")



// Setup DBG environment
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>

// Create a dbg version of new that provides more information
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#else
#define DBG_NEW new
#endif

// Remaining includes
#include <string>
#include <vector>
#include <Windows.h>
#undef min

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream> 
#include "ConsoleController.hpp"
using namespace std;



#if defined( _DEBUG ) && defined(_DLL ) // _DEBUG
#pragma comment (lib, "plib-mt-gd.lib" )
#elif defined( _DEBUG ) && !defined( _DLL ) // _DEBUG (Static)
#pragma comment (lib, "plib-mt-sgd.lib" )
#elif !defined( _DEBUG ) && defined( _DLL ) // Release
#pragma comment (lib, "plib-mt.lib" )
#elif !defined( _DEBUG ) && !defined( _DLL ) // Release (Static)
#pragma comment (lib, "plib-smt.lib" )
#endif



/*!	\brief	ConsoleApplication is the base-class of the framework.
*/
class ConsoleApplication {
	static ConsoleApplication* thisApp_sm;
	friend int wmain(int argc, wchar_t* argv[]);
	int wmain(int argc, wchar_t* argv[]);

	// System Data
	vector<char>			    originalTitle;
	HANDLE hConsoleInput, hConsoleOutput;
	CONSOLE_SCREEN_BUFFER_INFO	originalCSBI;	//old window/buffer size
	CONSOLE_CURSOR_INFO			originalCCI_;	//the cursor
	vector<CHAR_INFO>			originalBuffer;
	COORD						originalBufferCoord;
	DWORD						originalConsoleMode;
	WORD						currentConsoleWidth_ = 0;

protected:
	ConsoleApplication();
	virtual ~ConsoleApplication();
	virtual int execute();


public:
	int helper = BACKGROUND_RED;
	void saveConsoleState();

	void resizeWindow(WORD w, WORD h);

	void restoreWindow();

	// set the console's title
	inline void setTitle(LPCSTR title)
	{
		ConsoleController::instance().setTitle(title);
	}

	//pass a boolean value's pointer to controller from App which tells App when to stop
	inline void setFlag(bool* d)
	{
		ConsoleController::instance().setFlag(d);
	}

	//set background color of console
	inline void setBackGroundColor(WORD color, string area)
	{
		ConsoleController::instance().setBackGroundColor(color, area);
	}
	
	//set background color of console version2
	inline void setBackGroundColor2(WORD color, string area)
	{
		ConsoleController::instance().setBackGroundColor(color, area);
	}
	
	//set background color of console to white
	inline void setBackGroundColorWhite()
	{
		ConsoleController::instance().setBackGroundColorWhite();
	}
	
	//monitor user input and return the information version3
	inline tuple<char, short, short> startMonitoringInput3()
	{
		return ConsoleController::instance().startMonitoringInput3();
	}
	
	//monitor user input and return the information version0
	inline tuple<string, short, short> startMonitoringInput0()
	{
		return ConsoleController::instance().startMonitoringInput0();
	}
	
	//set background color of a cell on console(by string)
	inline void setCellBackGroundColor(string Color, short x, short y)
	{
		ConsoleController::instance().setCellBackGroundColor(Color, x, y);
	}
	
	//set background color of a cell on console (by color)
	inline void setCellBackGroundColor(WORD  Color, short x, short y)
	{
		ConsoleController::instance().setCellBackGroundColor(Color, x, y);
	}
	
	//print msg on screen
	inline void  printMsg(string msg, short x, short y)
	{
		ConsoleController::instance().printMsg(msg, x, y);
	}
	
	//set background color and character
	inline void  setBackGroundColorWithChar(string& msg)
	{
		ConsoleController::instance().setBackGroundColorWithChar(msg);
	}
	
	//put colorful strings on console
	inline void  setColorString(string& msg, vector<WORD> attr)
	{
		ConsoleController::instance().setColorString(msg,attr);
	}
	
	//display and area on console
	inline void drawView(COORD cursorHomeCoord, DWORD consoleSize, WORD view1Color)
	{
		ConsoleController::instance().drawView(cursorHomeCoord, consoleSize, view1Color);
	}
	
	//print string on console
	inline void OutputString(WORD x, WORD y, string const& s, WORD attribute, WORD currentConsoleWidth)
	{
		ConsoleController::instance().OutputString(x, y, s, attribute, currentConsoleWidth);
	}
	
	//print string on console
	inline void OutputString(WORD x, WORD y, string const& s, vector<WORD> attributes, WORD currentConsoleWidth)
	{
		ConsoleController::instance().OutputString(x, y, s, attributes, currentConsoleWidth);
	}
	
	//draw a edit control on console
	inline void DrawEditControl(COORD cursorHomeCoord, DWORD consoleSize, WORD EDIT_CONTROL_ATTR)
	{
		ConsoleController::instance().DrawEditControl(cursorHomeCoord, consoleSize, EDIT_CONTROL_ATTR);
	}

};