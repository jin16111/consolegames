#pragma once
/*!	\file		Command.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	Command class declaration. Command classes
*/
#include "ConsoleApplication.hpp"
#include "ViewInfo.hpp"

class Command
{
public:
	virtual void execute() {};
};


class CommandV2Obsolete
{
	// 1. Create a class that encapsulates an object and a member function
	// a pointer to a member function (the attribute's name is "method")
	ConsoleApplication *object; //    
	void(ConsoleApplication:: *method)();
public:
	CommandV2Obsolete(ConsoleApplication *obj = 0, void(ConsoleApplication:: *meth)() = 0);
	void execute();

};


class DrawAreaCommand :Command
{ 
public:
	ConsoleApplication* app;
	DrawAreaCommand();
	DrawAreaCommand(ConsoleApplication* newd);

	void execute(ViewInfo vi);
};

class DrawDotCommand :Command
{
	vector<pair<SHORT,SHORT>> history;
public:
	ConsoleApplication* app;
	DrawDotCommand() {}
	DrawDotCommand(ConsoleApplication* newd);

	void execute(ViewInfo vi);
	void undo();
};

class OutputStrCommand :Command
{ 
	vector<ViewInfo> history;
public:
	ConsoleApplication* app;
	OutputStrCommand() {}

	void execute(ViewInfo vi);
	void undo();

	//undo 2 executions at once
	void undo2();
};

class DrawEditControlCommand :Command
{
public:
	ConsoleApplication* app;
	DrawEditControlCommand() {}
	DrawEditControlCommand(ConsoleApplication* newd);

	void execute(ViewInfo vi);
};
