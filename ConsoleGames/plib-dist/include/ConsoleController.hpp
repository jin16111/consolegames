#pragma once
/*!	\file		ConsoleController.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	ConsoleController class declaration. ConsoleController is the inner facade which monitors and interacts with Win32 Console applicaion.
*/

#include <Windows.h>
#undef min

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <tuple>
#include "XError.hpp"
#include <memory>

using namespace std;



class ConsoleController
{
	HANDLE hConsoleInput, hConsoleOutput;
	// Colours
	WORD const FOREGROUND_BLACK = 0;
	WORD const FOREGROUND_WHITE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
	WORD const BACKGROUND_BLACK = 0;
	WORD const BACKGROUND_WHITE = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;



public:
	ConsoleController();
	static unique_ptr<ConsoleController> _instance;
	static bool* done;
	void setFlag(bool* d);

	//singleton
	static ConsoleController& instance();

	//monitor user input and return the information version3
	tuple<char, short, short> startMonitoringInput3();
	
	//monitor user input and return the information version0
	tuple<string, short, short> startMonitoringInput0();
	
	//monitor mouse input and return the information version3
	tuple<char, short, short>  mouseEventProc3(MOUSE_EVENT_RECORD const& mer);
	
	//monitor mouse input and return the information version0
	tuple<string, short, short> mouseEventProc0(MOUSE_EVENT_RECORD const & mer);
	
	//set background color of a cell on console(by string)
	void setCellBackGroundColor(string Color, short x, short y);
	
	//set background color of a cell on console (by color)
	void setCellBackGroundColor(DWORD Color, short x, short y);
	
	//set background color of console
	void setBackGroundColor(WORD color, string area);
	
	//set background color of console to white
	void setBackGroundColorWhite();
	
	//set background color of console with characters
	void setBackGroundColorWithChar(string& s);
	
	//put colorful strings on console
	void setColorString(string& msg, vector<WORD> attr);
	
	//print msg on screen
	void printMsg(string& msg, short x, short y);
	
	// set the console's title 
	void setTitle(LPCSTR title);
	
	//deal with Control + keys
	static BOOL CtrlHandler(DWORD ctrlType);
	
	//draw an area on screen
	void drawView(COORD cursorHomeCoord, DWORD consoleSize, WORD view1Color);
	
	//print string on console 
	void OutputString(WORD x, WORD y, string const& s, vector<WORD> const& attributes, WORD currentConsoleWidth);
	
	//print string on console 
	void OutputString(WORD x, WORD y, string const& s, WORD attribute, WORD currentConsoleWidth);

	//draw a edit control on console
	void DrawEditControl(COORD cursorHomeCoord, DWORD consoleSize, WORD EDIT_CONTROL_ATTR); 
};