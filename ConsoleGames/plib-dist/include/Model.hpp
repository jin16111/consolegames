#pragma once

/*!	\file		Model.hpp
	\author		Jinzhi Yang
	\date		2019-04-18

	 Model is the subject in observer pattern
*/

#include "View.hpp"
#include "ViewInfo.hpp"


class Model
{ 
public:
	std::vector<IObserver *> list;
	void attach(IObserver * view);
	void detach(IObserver * view);
	void notify(ViewInfo value);
	string query(int r, int c);
};

